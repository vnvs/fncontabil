import proccessBuy from './processBuy';

const mockList = [
  {
    type: 'BUY',
    id: 1,
    date: '2019-10-20',
    price: 15.2,
  },
  {
    type: 'BUY',
    id: 2,
    date: '2019-10-21',
    price: 20.2,
  },
];

const mockNewItem = {
  id: 3,
  date: '2019-10-30',
  price: 30,
};

const successfullList = [
  {
    type: 'BUY',
    id: 1,
    date: '2019-10-20',
    price: 15.2,
  },
  {
    type: 'BUY',
    id: 2,
    date: '2019-10-21',
    price: 20.2,
  },
  {
    type: 'BUY',
    id: 3,
    date: '2019-10-30',
    price: 30,
  },
];

describe('it should add the bought element', () => {
  it('Adds new item as buy', () => {
    const result = proccessBuy(mockNewItem, mockList);

    expect(result).toEqual(successfullList);
  });
});
