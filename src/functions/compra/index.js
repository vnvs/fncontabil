export const buyFunction = (prompt, list, rootQuestion) => {
  const schema = {
    properties: {
      id: {
        message: 'Qual id do tem?',
      },
      date: {
        message: 'Qual a data? (formato yyyy-mm-dd)',
      },
      price: {
        message: 'Qual valor?',
      },
    },
  };

  console.log(list);

  prompt.get(schema, function(err, result) {
    const newList = proccessBuy(result, list);
    rootQuestion(prompt, newList);
  });
};

export const proccessBuy = (item, list) => {
  const newItem = {
    ...item,
    type: 'BUY',
    price: Number(item.price),
    id: Number(item.id),
  };
  const newList = [...list, newItem];
  return newList;
};
