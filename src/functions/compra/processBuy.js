const proccessBuy = (item, list) => {
  const newItem = { ...item, type: 'BUY' };
  const newList = [...list, newItem];
  return newList;
};

export default proccessBuy;
