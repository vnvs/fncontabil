const prompt = require('prompt');
import { buyFunction } from './compra';
import sellFunction from './venda';
import showBook from './show';

export const rootQuestion = (prompt, list) => {
  const schema = {
    properties: {
      option: {
        message:
          'O que você quer? -- (1) Contas a pagar -- (2) Contas a receber -- (3) Mostrar livro',
      },
    },
  };

  prompt.get(schema, function(err, result) {
    const selectedOption = Number(result.option);
    switch (selectedOption) {
      case 1: {
        return buyFunction(prompt, list, rootQuestion);
      }
      case 2: {
        return sellFunction(prompt, list, rootQuestion);
      }
      case 3: {
        return showBook(prompt, list, rootQuestion);
      }
    }
  });
};

const main = () => {
  const firstList = [
    {
      type: 'BUY',
      id: 1,
      date: '2019-10-20',
      price: 15.2,
    },
    {
      type: 'BUY',
      id: 2,
      date: '2019-10-21',
      price: 20.2,
    },
    {
      type: 'SELL',
      id: 5,
      date: '2019-10-22',
      price: 25.5,
    },
    {
      type: 'BUY',
      id: 10,
      date: '2019-10-23',
      price: 15.2,
    },
    {
      type: 'SELL',
      id: 12,
      date: '2019-10-24',
      price: 1015.2,
    },
    {
      type: 'BUY',
      id: 4,
      date: '2019-10-25',
      price: 935.2,
    },
    {
      type: 'SELL',
      id: 3,
      date: '2019-10-26',
      price: 55.2,
    },
  ];

  rootQuestion(prompt, firstList);
};

main();
