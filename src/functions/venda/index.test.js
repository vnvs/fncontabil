import sellFunction from './index';

const mockRootQuestion = jest.fn();

describe('it should add the sould element', () => {
  const mockPrompt = {
    get(err, result) {
      result(null, 'message');
    },
  };

  it('Adds new item as buy', () => {
    sellFunction(mockPrompt, [], mockRootQuestion);
    expect(mockRootQuestion).toHaveBeenCalled();
  });
});
