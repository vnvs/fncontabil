import proccessSell from './processSell';

const sellFunction = (prompt, list, rootQuestion) => {
  const schema = {
    properties: {
      id: {
        message: 'Qual id do tem?',
      },
      date: {
        message: 'Qual a data? (formato yyyy-mm-dd)',
      },
      price: {
        message: 'Qual valor?',
      },
    },
  };

  prompt.get(schema, function(err, result) {
    const newList = proccessSell(result, list);
    rootQuestion(prompt, newList);
  });
};

export default sellFunction;
