const proccessSell = (item, list) => {
  const newItem = {
    ...item,
    type: 'SELL',
    price: Number(item.price),
    id: Number(item.id),
  };
  const newList = [...list, newItem];
  return newList;
};

export default proccessSell;
