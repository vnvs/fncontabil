import proccessSell from './processSell';

const mockList = [
  {
    type: 'SELL',
    id: 1,
    date: '2019-10-20',
    price: 15.2,
  },
  {
    type: 'SELL',
    id: 2,
    date: '2019-10-21',
    price: 20.2,
  },
];

const mockNewItem = {
  id: 3,
  date: '2019-10-30',
  price: 30,
};

const successfullList = [
  {
    type: 'SELL',
    id: 1,
    date: '2019-10-20',
    price: 15.2,
  },
  {
    type: 'SELL',
    id: 2,
    date: '2019-10-21',
    price: 20.2,
  },
  {
    type: 'SELL',
    id: 3,
    date: '2019-10-30',
    price: 30,
  },
];

describe('it should add the sould element', () => {
  it('Adds new item as buy', () => {
    const result = proccessSell(mockNewItem, mockList);

    expect(result).toEqual(successfullList);
  });
});

