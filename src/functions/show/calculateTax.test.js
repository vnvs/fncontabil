import calculateTax from './calculateTax';

const mockItems = {
  1: { name: 'Mesa', tax: 0.15 },
  2: { name: 'Cadeira', tax: 0.13 },
  3: { name: 'Computador', tax: 0.23 },
  4: { name: 'Agua', tax: 0.41 },
  5: { name: 'Telefone', tax: 0.05 },
  6: { name: 'Cola', tax: 0.03 },
  7: { name: 'Monitor', tax: 0.02 },
  8: { name: 'Cafe', tax: 0.09 },
  9: { name: 'Impressora', tax: 0.14 },
  10: { name: 'Pilha', tax: 0.13 },
  11: { name: 'Cadeira', tax: 0.17 },
  12: { name: 'Cartucho', tax: 0.43 },
  13: { name: 'Impressao', tax: 0.33 },
  14: { name: 'Caixa de som', tax: 0.11 },
  15: { name: 'Cd', tax: 0.11 },
  16: { name: 'Pen drive', tax: 0.04 },
  17: { name: 'Cabo de rede', tax: 0.08 },
  18: { name: 'Roteador', tax: 0.18 },
  19: { name: 'Caneta', tax: 0.28 },
  20: { name: 'Lapis', tax: 0.01 },
};

const mockList = [
  {
    type: 'BUY',
    id: 1,
    date: '2019-10-20',
    price: 215.2,
  },
  {
    type: 'BUY',
    id: 2,
    date: '2019-10-21',
    price: 120.2,
  },
  {
    type: 'SELL',
    id: 5,
    date: '2019-10-22',
    price: 525.5,
  },
  {
    type: 'BUY',
    id: 10,
    date: '2019-10-23',
    price: 315.2,
  },
  {
    type: 'SELL',
    id: 12,
    date: '2019-10-24',
    price: 21015.2,
  },
  {
    type: 'BUY',
    id: 4,
    date: '2019-10-25',
    price: 2935.2,
  },
  {
    type: 'SELL',
    id: 3,
    date: '2019-10-26',
    price: 155.2,
  },
];

describe('CalculateValues', () => {
  it('Calculates the right value', () => {
    expect(calculateTax(mockList, mockItems)).toEqual('9098.51');
  });
});
