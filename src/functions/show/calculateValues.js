const calculateValues = list => {
  const opts = list.reduce(
    (acc, item) => {
      if (item.type == 'SELL') {
        return { ...acc, SELL: acc.SELL + item.price };
      } else {
        return { ...acc, BUY: acc.BUY + item.price };
      }
    },
    { SELL: 0, BUY: 0 },
  );

  const result = {
    BUY: opts.BUY.toFixed(2),
    SELL: opts.SELL.toFixed(2),
    total: (opts.SELL - opts.BUY).toFixed(2),
  };

  return result;
};

export default calculateValues;
