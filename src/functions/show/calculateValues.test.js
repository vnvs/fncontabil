import calculateValues from './calculateValues';

const mockList = [
  {
    type: 'BUY',
    id: 1,
    date: '2019-10-20',
    price: 215.2,
  },
  {
    type: 'BUY',
    id: 2,
    date: '2019-10-21',
    price: 120.2,
  },
  {
    type: 'SELL',
    id: 5,
    date: '2019-10-22',
    price: 525.5,
  },
  {
    type: 'BUY',
    id: 10,
    date: '2019-10-23',
    price: 315.2,
  },
  {
    type: 'SELL',
    id: 12,
    date: '2019-10-24',
    price: 21015.2,
  },
  {
    type: 'BUY',
    id: 4,
    date: '2019-10-25',
    price: 2935.2,
  },
  {
    type: 'SELL',
    id: 3,
    date: '2019-10-26',
    price: 155.2,
  },
];

describe('CalculateValues', () => {
  it('Calculates the right value', () => {
    expect(calculateValues(mockList)).toEqual({
      BUY: '3585.80',
      SELL: '21695.90',
      total: '18110.10',
    });
  });
});
