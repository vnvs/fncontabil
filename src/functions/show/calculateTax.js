const calculateTax = (list, items) => {
  const tax = list.reduce((acc, item) => {
    if (item.type == 'SELL') {
      return (
        Number(acc || 0) + Number(item.price * items[item.id].tax)
      );
    } else {
      return acc;
    }
  }, 0);

  return tax.toFixed(2);
};

export default calculateTax;
