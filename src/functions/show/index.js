import items from '../../items/items';
import { mainQuestion } from '../index';
import calculateValues from './calculateValues';
import generateList from './generateList';
import calculateTax from './calculateTax';

const showBook = (prompt, list, rootQuestion) => {
  const formatedList = generateList(list, items);
  const totalTax = calculateTax(list, items);
  const values = calculateValues(list).total;

  console.table(formatedList);
  console.log();
  console.log(`resultado: $${values}`);
  console.log(`Imposto devido: $${totalTax}`);

  rootQuestion(prompt, list);
};

export default showBook;
