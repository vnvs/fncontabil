const generateList = (list, items) => {
  const newList = list.map(item => {
    return {
      type: item.type,
      name: items[item.id].name,
      date: item.date,
      price: item.price,
    };
  });

  return newList;
};

export default generateList;
