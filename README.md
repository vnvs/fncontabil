Você precisa ter ao menos

node v10.15.0
npm v6.4.1
jest v24.8.0

Dentro da pasta do sistema execute para instalar dependências: npm install
Para iniciar o projeto, execute: npm start
Para executar os testes execute: npm test
